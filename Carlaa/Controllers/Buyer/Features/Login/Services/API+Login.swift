//
//  API+Login.swift
//  Carlaa
//
//  Created by Wong Jin Lun on 14/06/2022.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper

class Login {
    
     func APIHeaders() -> HTTPHeaders {
        var accessToken = UserDefaults.standard.string(forKey: Constants.Cache.AccessToken)
        if accessToken == nil {
            accessToken = ""
        }
        return ["Authorization": "Bearer \(accessToken!)"]
    }
    
    var callBack:userCallBack?
    fileprivate var baseUrl = ""
    typealias userCallBack = (_ user:[User]?, _ status: Bool, _ message:String) -> Void
    
    init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    //MARK:- getUserInfo
    func getUserInfo(endPoint:String) {
        AF.request(self.baseUrl + endPoint, method: .get, parameters: nil, encoding: URLEncoding.default, headers: APIHeaders(), interceptor: nil).response { (responseData) in
            guard let data = responseData.data else {
                self.callBack?(nil, false, "")

                return}
            do {
            let countries = try JSONDecoder().decode([User].self, from: data)
                self.callBack?(countries, true,"")
            } catch {
                self.callBack?(nil, false, error.localizedDescription)
            }
            
        }
    }
    
    func completionHandler(callBack: @escaping userCallBack) {
        self.callBack = callBack
    }
    
   

}
