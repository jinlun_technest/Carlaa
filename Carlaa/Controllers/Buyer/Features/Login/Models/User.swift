import Foundation

struct User: Decodable {
    var name: String?
    var capital: String?
    var countryCode:String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case capital = "capital"
        case countryCode = "alpha3Code"
    }
    
}
