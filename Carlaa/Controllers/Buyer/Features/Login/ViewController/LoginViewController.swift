//
//  LoginController.swift
//  Carlaa
//
//  Created by Wong Jin Lun on 14/06/2022.
//

import UIKit

class LoginViewController: UIViewController {

    var user = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getUserInfo()
    }
    
    func getUserInfo() {
        let service = Login(baseUrl: "https://asdasdasd.co/web/user/")
        service.getUserInfo(endPoint: "all")
        service.completionHandler { [weak self] (user, status, message) in
            if status {
                guard let self = self else {return}
                guard let _user = user else {return}
                self.user = _user
               
            }
        }
    }

}
