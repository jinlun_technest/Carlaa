//
//  Utilities.swift
//  Carlaa
//
//  Created by Wong Jin Lun on 15/06/2022.
//

import UIKit

class Utilities {

    static func removeDot(dotStr: String) -> String {
        let newString = dotStr.replacingOccurrences(of: ".", with: "", options: .literal, range: nil)
        return newString
    }

}

extension Double {
    // Rounds the double to decimal places value
    func rounded(toPlaces places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
